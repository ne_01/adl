print "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\">";
print "<html>";
print "<head>";
print "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\">";
print "<title>ADL Scenario Source Code</title>";
print "</head>";
print "<body>";
print "<pre>";
while (<>) {
    s///g;
    s///g;
    s/&/&amp;/g;
    s/</&lt;/g;
    s/>/&gt;/g;
    s/(\$[a-zA-Z0-9]*)/<b>$1<\/b>/g;
    s/(\/\/.*)$/<i>$1<\/i>/g;
    s/\/\*/<i>\/\*/g;
    s/\*\//\*\/<\/i>/g;
    s/(^|[^a-zA-Z0-9])(INCLUDE|MESSAGE|VAR|ROUTINE|LOCAL|BITPROP|PROPERTY|ARTICLE|PREP|ADJEC|NOUN|VERB|DWIMD|DWIMI|START|NOVERB|TELLER|LDESC|SDESC|ACTION|PREACT|Verb|Dobj|Iobj|Prep|Conj|Numd|STRING|WHILE|DO|IF|THEN|ELSEIF|ELSE|If|Else|While|Proc|Break|Return|Var)($|[^a-zA-Z0-9])/$1<b>$2<\/b>$3/g;
    print;
}
print "</pre>";
print "</body>";
print "</html>";
