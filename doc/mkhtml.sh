#!/bin/sh
for i in $(cd ../samples;find . -name '*.adl')
do
    newname=$(echo $i|sed 's/adl$/html/')
    perl adl2html.pl <../samples/$i >samples/$newname
done
