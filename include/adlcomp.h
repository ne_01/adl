/***************************************************************\
*								*
*	adlcomp.h - type, variable, function, and macro defs	*
*	for use by adlcomp.					*
*								*
\***************************************************************/

#define EXP_CONST 0 /* Info == constant, 0 args		*/
#define EXP_CALL 1  /* Info = args + 1 (arg[0] is rout)	*/
#define EXP_DOT 2   /* Info = 2 (arg[0]=obj, arg[1]=prop)	*/
#define EXP_INDEX 3 /* Info = 2 (arg[0]=base, arg[1]=idx)	*/
#define EXP_VAR 4   /* Info = offset, 0 args		*/
#define EXP_ARG 5   /* Info = offset, 0 args		*/
#define EXP_LOCAL 6 /* Info = offset, 0 args		*/
#define EXP_ME 7    /* Info = 0				*/
#define EXP_ADDR 8  /* Info = 1, arg[0]=what to take addr	*/
#define EXP_DEREF 9 /* Info = 1, arg[0]=what to deref	*/

typedef struct _Expr {
    int16 exprType;            /* From EXP_*, above			*/
    int16 exprInfo;            /* Constant, offset, or count		*/
    struct _Expr *exprArgs[1]; /* Variable length array of args	*/
} * Expr;

extern char token[], /* Current token found by lexer		*/
  inname[];          /* Name of the current input file	*/

extern int16 t_val, /* Val of token from dict		*/
  t_type,           /* Type of token from dict		*/
  numerr,           /* Number of errors found so far	*/
  numwarn,          /* Number of warnings found so far	*/
  numline,          /* Number of lines encountered		*/
  wignore,          /* Ignore warnings?			*/
  compat,           /* Compatibility mode for old ADL progs */
  inproc,           /* Inside a Proc() def */
  maxerr,           /* Maximum number of errors allowed	*/
  numbits,          /* # of bitprops		*/
  numprops,         /* # of properties		*/
  NUM_VARS,         /* Default # of ADL variables	*/
  NUM_ROUTS,        /* Default # of ADL routines	*/
  NUM_OBJS,         /* Default # of ADL objects	*/
  NUM_VERBS,        /* Default # of ADL verbs	*/
  NUM_STR,          /* Default # of ADL strings	*/
  NUM_PREP,         /* Default # of prep synonyms	*/
  NUM_VS,           /* Default # of verb synonyms	*/
  NUM_BP,           /* Default # of bitprops	*/
  NUM_PROP;         /* Default # of properties	*/

extern char *REDECLARATION, /* Variable redeclaration		*/
  *BAD_ARRAY,               /* Array size too small			*/
  *BRACKET_EXPECTED,        /* ']' expected				*/
  *LEFT_EXPECTED,           /* '(' expected				*/
  *RIGHT_EXPECTED,          /* ')' expected				*/
  *SEMI_EXPECTED,           /* ';' expected				*/
  *COMMA_EXPECTED,          /* ',' expected				*/
  *NOUN_WANTED,             /* Noun expected in expression		*/
  *CONST_EXPECTED,          /* Constant expected in expression	*/
  *VAR_EXPECTED,            /* VAR expected in expression		*/
  *EQUAL_EXPECTED,          /* '=' expected				*/
  *PREP_EXPECTED,           /* Prep expected in expression		*/
  *ATTEMPT,                 /* Attempt to redefine a noun		*/
  *ILLEGAL_SYMBOL;          /* Illegal symbol (syntax error)	*/

extern int32 bitpat[], /* Bit masks for bit properties		*/
  ibitpat[];           /* Same as above, but inverted.		*/

extern FILE *CODE_F; /* Temporary file for paging code	*/

/* From adlcomp.c */
extern void breaker(void); /* Exit from ADLCOMP after fixing files	*/

/* From adllex.c */
extern void lexer(void),       /* Gets a new token			*/
  unlex(void),                 /* Ungets the token so it can be reread	*/
  emit_file(void);             /* Emit the FILEN and LINEN opcodes	*/
extern int open_input(char *); /* Open a new input file		*/
extern void close_input(void), /* Close the input file			*/
  save_input(void **),         /* Save the input file...		*/
  restore_input(void *);       /* Restore the input file...		*/

/* From adlmisc.c */
extern void getvars(void),   /* Get VAR a, b, c...			*/
  getlist(int16),            /* Get a list of things (like ADJECs)	*/
  getverb(void),             /* Get a verb property assignment	*/
  globassign(void),          /* Get a global assignment		*/
  routassign(void),          /* Get a routine assignment		*/
  prepassign(void),          /* Get a prepositional assignment	*/
  getverbsyn(int16);         /* Get a verb synonym			*/
extern int16 getassign(int); /* Get assignment statement		*/

/* From codegen.c */
extern address newcode(char, int16), /* Add a new instruction to CODE_F	*/
  oldcode(address, char, int16),     /* Modify an old instruction	*/
  currcode(void);                    /* Return the current end of CODE_F	*/

/* From adlmsg.c */
extern void eatuntil(int16), error(char *), warning(char *), fatal(char *);

#ifdef RANGE_CHECKING
extern void toomany(char *);
#endif

/* From adlobj.c */
extern int16 noun_exists(int16, int16), /* Return object ID of adj-noun pair */
  new_noun(int16, int16),               /* Create a new object */
  getnew(int16),                    /* Get an undeclared object from input	*/
  getold(int16, int16);             /* Get previously declared obj from input */
extern void move_obj(int16, int16), /* Move an object to a new location */
  setprop(int16, int16, int16),     /* Set a property of an object */
  nounassign(int16, int16),         /* Parse/interpret property assignment */
  getnouns(void);                   /* Parse/interpret a NOUN decl */

/* From adlproc.c */
extern void getproc(void);

/* From compdict.c */
extern int16 lookup(char *, int16 *); /* Find the type and value of a token
                                         */
extern void insertkey(char *, int16, int16,
                      int16), /* Make a new token, given type
                                 and val	*/
  insert_sys(char *, int16, int16),
  insert_local(char *, int16, int16);
extern void count_symtab(int), write_symtab(FILE *, int), del_locals(void);

/* From predef.c */
extern void init_predefs(void); /* Initialize the symbol table		*/

/* From routcomp.c */
extern void getglob(void), getpair(void), getexpr(int16), getform(void),
  getwhile(void), getif(void), getroutine(int16);
extern int16 getargs(void);

/* Error macros */
#define _ERR_FIX(str, ch)                                                      \
    {                                                                          \
        error(str);                                                            \
        eatuntil(ch);                                                          \
        return;                                                                \
    }
#define _ERR_FX0(str, ch)                                                      \
    {                                                                          \
        error(str);                                                            \
        eatuntil(ch);                                                          \
        return 0;                                                              \
    }

/*** EOF adlcomp.h ***/
