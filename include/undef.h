/* Undefine any predefined things */
#ifdef HPUX
#undef HPUX
#endif

#ifdef MSDOS
#undef MSDOS
#endif

#ifdef BSD
#undef BSD
#endif

#ifdef AMIGA
#undef AMIGA
#endif

#ifdef PCC
#undef PCC
#endif

#ifdef LATTICE
#undef LATTICE
#endif

#ifdef ERRCHECK
#undef ERRCHECK
#endif

#ifdef HPTERM
#undef HPTERM
#endif

#ifdef ANSI
#undef ANSI
#endif

#ifdef TERMCAP
#undef TERMCAP
#endif

#ifdef UNIX
#undef UNIX
#endif

/*** EOF undef.h ***/
