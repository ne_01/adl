#include "undef.h" /* Undefine any predefined things */

/***************************************************************\
*								*
*	The following #defines configure the ADL system for	*
*	local machine architectures and operating systems.	*
*	Read them carefully, then appropriately define the	*
*	constants.  If your system doesn't exist here, make	*
*	up a new name and create a new set of typedefs		*
*	and system-dependant #defines for that system.		*
*								*
\***************************************************************/

#ifdef WIN32
#define MSDOS 1
#else
#define HPUX 1  /* HP-UX or SysIII or SysV UN*X	*/
#define MSDOS 0 /* MS-DOS 2.1 or greater	*/
#define BSD 0   /* BSD 4.2 or greater		*/
#define AMIGA 0 /* Amiga computer		*/
#endif

/* Language defines */
#define PCC 1     /* UN*X-style C compiler	*/
#define LATTICE 0 /* Lattice C			*/

/* Utility defines */
#define ERRCHECK 1       /* Verbose error check		*/
#define RANGE_CHECKING 1 /* Range checking in ADLCOMP	*/
#define LONG_ADDRESS 1   /* 19-bit addresses (512k code)	*/

/* Terminal defines */
#define HPTERM 0  /* HP terminal I/O		*/
#define ANSI 0    /* ANSI terminal I/O		*/
#define TERMCAP 0 /* Termcap I/O			*/
#define CURSES 1  /* Use libcurses                */

/* The following is for BSD-like systems - it allows for the
   execution of an ADL binary directly.  Change it to reflect
   where adlrun lives on your system.
*/

#ifndef ADL_NAME
#define ADL_NAME "/usr/games/adlrun"
#endif

/* The following is the character which is used to obfuscate strings */
#define CODE_CHAR 0xff

#if HPUX
#define UNIX 1
typedef short int16;
typedef long int32;
#if LONG_ADDRESS
typedef long address;
#else
typedef unsigned short address;
#endif
#define SRAND srand
#define RAND rand()
#endif

#if BSD
#define UNIX 1
typedef short int16;
typedef long int32;
#if LONG_ADDRESS
typedef long address;
#else
typedef unsigned short address;
#endif
#define SRAND srandom
#define RAND random()
#endif

#if AMIGA
#define UNIX 0
typedef short int16;
typedef long int32;
#if LONG_ADDRESS
typedef long address;
#else
typedef unsigned short address;
#endif
#define SRAND srand
#define RAND rand()
#define time mytime
extern int32 mytime(int32 *);
#endif

#if MSDOS
#define UNIX 0
#define int16 short
#define int32 long
#if LONG_ADDRESS
#define address long
#else
#define address unsigned
#endif
#define SRAND srand
#define RAND rand()
#define time mytime

extern int32 mytime(int32 *);
#endif

extern void checkbreak(void (*rout)(void));

/*** EOF adltypes.h ***/
