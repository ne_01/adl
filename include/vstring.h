extern int16 newstr(char *str), /* Sets up str as a new permanent string */
  newtstr(char *str),           /* Sets up str as a new temporary string */
  vs_save(int16 n),             /* Sets up virtstr( int16 ) as a new "CMOS"
                                   string */
  numstr(void);                 /* Returns the number of permanent strings
                                   defined */

extern int32 numchar(void);        /* Returns the number of permanent chars
                                      allocated */
extern char *virtstr(int16 index); /* Returns the virtstr at index */

extern void vsflush(void), encode(char *), decode(char *),
  vsinit(FILE *, int32, int16, char *, char *, int16 *, int32 *);

/*** EOF vstring.h ***/
