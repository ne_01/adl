/* This table *MUST* be kept in-sync with builtins.h */

char *adlBuiltins[] = {
  "", /* Nothing at 0 */

  /* Object functions */

  "$loc",    /* ($loc OBJ) container of OBJ */
  "$cont",   /* ($cont OBJ) First item contained in OBJ */
  "$link",   /* ($link OBJ) Next obj in same node as OBJ */
  "$ldesc",  /* ($ldesc OBJ) LDesc of OBJ */
  "$sdesc",  /* ($sdesc OBJ) SDesc of OBJ */
  "$action", /* ($action OBJ) Action routine for OBJ */
  "$prop",   /* ($prop OBJ NUM) NUM'th prop of OBJ */
  "$setp",   /* ($setp OBJ NUM VAL) OBJ(NUM) = VAL */
  "$move",   /* ($move OBJ1 OBJ2)  move OBJ1 to OBJ2 */
  "$modif",  /* ($modif OBJ) modifier of OBJ -
                        if < 0, it is a verb; if > 0 it
                        is an adjec, otherwise, it's null */

  /* Verb functions */

  "$vset",  /* ($vset VERB PROP VAL) VERB(PROP) = VAL */
  "$vprop", /* ($vprop VERB PROP)	 returns VERB(PROP) */

  /* Arithmetic functions */

  "$plus",  /* ($plus A B) A + B */
  "$minus", /* ($minus A B) A - B */
  "$times", /* ($times A B) A * B */
  "$div",   /* ($div A B) A / B */
  "$mod",   /* ($mod A B) A % B */
  "$rand",  /* ($rand N) Random # from 1 to N, inclusive */

  /* Boolean functions */

  "$and",  /* ($and A B) A & B */
  "$or",   /* ($or A B) A | B */
  "$not",  /* ($not N) 1 if N==0, 0 otherwise */
  "$yorn", /* ($yorn) (user input)[0] in [ 'y', 'Y' ] */
  "$pct",  /* ($pct N) 1 N% of the time */
  "$eq",   /* ($eq A B) A == B */
  "$ne",   /* ($ne A B) A != B */
  "$lt",   /* ($lt A B) A < B */
  "$gt",   /* ($gt A B) A > B */
  "$le",   /* ($le A B) A <= B */
  "$ge",   /* ($ge A B) A >= B */

  /* Miscellaneous Routines */

  "$say",    /* ($say A B ...) printf( "%s%s...", A,B,...)*/
  "$arg",    /* ($arg N) Nth arg to this routine */
  "$exit",   /* ($exit N) pop stack; if N !=0 next turn */
  "$return", /* ($return V) pop stack, retval = V */
  "$val",    /* ($val E) returns E */
  "$phase",  /* ($phase) returns current phase # */
  "$spec",   /* ($spec CODE A B C ...) perform one of:
                        CODE = 1, Toggle debugging mode
                        CODE = 2, Restart this run of ADL
                        CODE = 3, Terminate this run of ADL
                        CODE = 4, Save the game
                        CODE = 5, Restore a game
                        CODE = 6, Execute a program A with args B...
                        CODE = 7, Set the unknown words file
                        CODE = 8, Set script file
                        CODE = 9, Write a header
                        CODE = 10, Set left & right margins
                     */

  /* Global-value functions */

  "$setg",   /* ($setg VAR VAL) (VAR) = VAL */
  "$global", /* ($global VAR) @VAR */
  "$verb",   /* ($verb) @Verb */
  "$dobj",   /* ($dobj) @Dobj */
  "$iobj",   /* ($iboj) @Iobj */
  "$prep",   /* ($prep) @Prep */
  "$conj",   /* ($conj) @Conj */
  "$numd",   /* ($numd) @Numd */

  /* Transition procedures */

  "$setv", /* ($setv V1 .. V10) VECVERB = V1 .. V10 */
  "$hit",  /* ($hit OBJ D1 .. D10)
                           ($move OBJ D[ pos( @Verb, VECVERB )) */
  "$miss", /* ($miss R1 .. R10)
                           eval( R[ pos( @Verb, VECVERB ) ) */

  /* String functions */

  "$eqst",    /* ($eqst A B) returns strcmp( A, B ) == 0 */
  "$subs",    /* ($subs S P N) returns copy( S, P, N ) */
  "$leng",    /* ($leng S) returns length( S ) */
  "$cat",     /* ($cat S1 S2) returns strcat( S1, S2 ) */
  "$pos",     /* ($pos S1 S2) returns strpos( S1, S2 ) */
  "$chr",     /* ($chr N) returns '\NNN' */
  "$ord",     /* ($ord S) returns (int16) S[ 0 ] */
  "$read",    /* ($read) returns user input string */
  "$savestr", /* ($savestr S) saves S in perm. area */
  "$name",    /* ($name OBJ) returns (2 word) name of OBJ */
  "$vname",   /* ($vname VERB) returns name of VERB */
  "$mname",   /* ($mname MODIF) returns name of MODIF */
  "$pname",   /* ($pname PREP) returns name of PREP */
  "$define",  /* ($define a b) expands a to b at runtime */
  "$undef",   /* ($undef S) undefines S */
  "$str",     /* ($str N) returns the ascii value of N */
  "$num",     /* ($num S) returns numeric value of S */

  /* Demons, fuses, and actors */

  "$sdem",    /* ($sdem R) activates R as a demon */
  "$ddem",    /* ($ddem R) deactivates R as a demon */
  "$sfus",    /* ($sfus R N) Activates R as a fuse,
                              burning down in N turns */
  "$dfus",    /* ($dfus R) Quenches R as a fuse */
  "$incturn", /* ($incturn) Increment the turn counter */
  "$turns",   /* ($turns) Current val. of turn counter */
  "$prompt",  /* ($prompt R) Sets R as the prompt routine */
  "$actor",   /* ($actor OBJ STR FLAG) new actor */
  "$delact",  /* ($delact OBJ) Deletes actor OBJ */
};
