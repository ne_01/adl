/***************************************************************\
*								*
*	global.c - global variables for adlcomp.		*
*	Copyright 1987 by Ross Cunniff.  All rights reserved.	*
*								*
\***************************************************************/

#include "adlprog.h"
#include "adltypes.h"

struct header hdr;         /* Actual header		*/
struct verbrec *verbspace; /* Verb contents		*/
struct objrec *objspace;   /* Object contents		*/
struct preprec *prepspace; /* Preposition contents		*/
struct vp_syn *verbsyn;    /* Verb synonyms		*/
int16 *varspace;           /* Stack & variables		*/
int16 *nounspace;          /* Noun indices			*/
address *routspace;        /* Routine indexes		*/
int32 *str_tab;            /* String table			*/

int32 bitpat[32] = {0x00000001L, 0x00000002L, 0x00000004L, 0x00000008L,
                    0x00000010L, 0x00000020L, 0x00000040L, 0x00000080L,
                    0x00000100L, 0x00000200L, 0x00000400L, 0x00000800L,
                    0x00001000L, 0x00002000L, 0x00004000L, 0x00008000L,
                    0x00010000L, 0x00020000L, 0x00040000L, 0x00080000L,
                    0x00100000L, 0x00200000L, 0x00400000L, 0x00800000L,
                    0x01000000L, 0x02000000L, 0x04000000L, 0x08000000L,
                    0x10000000L, 0x20000000L, 0x40000000L, 0x80000000L},

      ibitpat[32] = {0xFFFFFFFEL, 0xFFFFFFFDL, 0xFFFFFFFBL, 0xFFFFFFF7L,
                     0xFFFFFFEFL, 0xFFFFFFDFL, 0xFFFFFFBFL, 0xFFFFFF7FL,
                     0xFFFFFEFFL, 0xFFFFFDFFL, 0xFFFFFBFFL, 0xFFFFF7FFL,
                     0xFFFFEFFFL, 0xFFFFDFFFL, 0xFFFFBFFFL, 0xFFFF7FFFL,
                     0xFFFEFFFFL, 0xFFFDFFFFL, 0xFFFBFFFFL, 0xFFF7FFFFL,
                     0xFFEFFFFFL, 0xFFDFFFFFL, 0xFFBFFFFFL, 0xFF7FFFFFL,
                     0xFEFFFFFFL, 0xFDFFFFFFL, 0xFBFFFFFFL, 0xF7FFFFFFL,
                     0xEFFFFFFFL, 0xDFFFFFFFL, 0xBFFFFFFFL, 0x7FFFFFFFL};

/*** EOF global.c ***/
