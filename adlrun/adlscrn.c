/***************************************************************\
*								*
*	adlscrn.c - Screen I/O for adlrun.  Add new def's	*
*	for a new terminal here (unless using termcap).		*
*								*
\***************************************************************/

#include <stdio.h>

#include "adldef.h"
#include "adlprog.h"
#include "adlrun.h"
#include "adltypes.h"
#include "vstring.h"

#if CURSES
#include "curses.h"
#endif

void sayer(char *s) {
    char word[SLEN];

    while (*s) {
        getchunk(&s, word);
        if (*word == '\n') {
            crlf();
            continue;
        } else if ((numcol + strlen(word)) > scrwidth) {
            while (*word == ' ') { /* Eat up blanks */
                getchunk(&s, word);
            }
            if (!*word) {
#if CURSES
                if (header) {
                    refresh();
                } else
#endif
                {
                    fflush(stdout);
                }
                return; /* The string was blank terminated */
            }
            crlf();
        }
        numcol += strlen(word);
#if CURSES
        if (header) {
            addstr(word);
        } else
#endif
        {
            fputs(word, stdout);
        }
        if (scriptfile != (FILE *)0) { fputs(word, scriptfile); }
    }

#if CURSES
    if (header) {
        refresh();
    } else
#endif
    {
        fflush(stdout);
    }
}

void crlf(void) {
#if CURSES
    if (header) {
        addch('\n');
    } else
#endif
    {
        putchar('\n');
    }
    if (scriptfile != (FILE *)0) { putc('\n', scriptfile); }
    numcol = 0;
}

void getstring(char *s) {
#if CURSES
    if (header) {
        getstr(s);
    } else
#endif
    {
        if (!fgets(s, 80, stdin)) { /* EOF detected! */
            head_term();
            exit(-1); /* Exit program */
        }
        /* Chop off trailing newline */
        if (s[strlen(s) - 1] == '\n') { s[strlen(s) - 1] = 0; }
    }

    if (scriptfile != (FILE *)0) { fprintf(scriptfile, "%s\n", s); }
    numcol = 0;
}

#if CURSES /* [ */

void head_setup(void) {
    if (header) {
        (void)initscr();
        scrollok(stdscr, 1);
        echo();
        clear();
        move(0, 0);
        standout();
        printw(H_STR, "", 0, 0); /* Header line */
        standend();
        move(LINES - 1, 0);
        refresh();
    }
}

void write_head(void) {
    if (header) {
        move(0, 0);
        standout();
        printw(H_STR, virtstr(ARG(2)), ARG(3), ARG(4));
        standend();
        move(LINES - 1, 0);
        refresh();
    }
}

void head_term(void) {
    if (header) { endwin(); }
}

#else /* ] [ */

#if HPTERM
static char TGOTO[] = "\033&a%dy%dC", CLEAR[] = "\033J", STANDOUT[] = "\033&dJ",
            STANDEND[] = "", LOCK[] = "\033l", NOLOCK[] = "\033m";
#endif

#if ANSI
static char TGOTO[] = "\033[%02d;%02dH", CLEAR[] = "\014",
            STANDOUT[] = "\033[0;7m", STANDEND[] = "\033[0m", LOCK[] = "",
            NOLOCK[] = "";
#endif

#if TERMCAP
char *BC, *UP; /* Just to satisfy the linker */

static char TGOTO[20], /* cm		*/
  CLEAR[10],           /* cd		*/
  STANDOUT[10],        /* so		*/
  STANDEND[10],        /* se		*/
  LOCK[10],            /* ml		*/
  NOLOCK[10];          /* mu		*/

static char *buffptr = NULL;
#endif

static int END = -1; /* Last line on the screen */

#if TERMCAP
static int mygetstr(char *which, char *where, int need);
#endif

void head_setup(void) {
    char *value; /* Value of an environment variable */
    FILE *outfile;

    if (!header) { return; }

    outfile = stdout;

#if TERMCAP
    /* Initialize termcap */
    if ((value = getenv("TERM")) == (char *)0) { error(ADLERR_TERMCAP); }
    if (tgetent(buffptr, value) <= 0) { error(ADLERR_TERMCAP); }

    /* Get the number of lines on the screen */
    END = tgetnum("li");

    /* Get the command strings */
    (void)mygetstr("cm", TGOTO, 1);
    (void)mygetstr("so", STANDOUT, 0);
    (void)mygetstr("se", STANDEND, 0);
    if (mygetstr("cd", CLEAR, 0) == 0) (void)mygetstr("cl", CLEAR, 1);
    (void)mygetstr("ml", LOCK, 0);
    (void)mygetstr("mu", NOLOCK, 0);
#endif

#if AMIGA
    /* Set the screen dimensions for a standard console window */
    END = 22;
#else
    /* See if the END is in the environment */
    if ((value = getenv("LINES")) != (char *)0) { END = atoi(value); }

    if (END < 0) {
        /* Assume a standard size terminal */
        END = 23;
    } else {
        /* Last line is number of lines minus one. */
        END--;
    }
#endif

/* Go to the top of the screen */
#if TERMCAP
    fprintf(outfile, (char *)tgoto(TGOTO, 0, 0));
#endif
#if ANSI
    fprintf(outfile, TGOTO, 1, 1);
#endif
#if HPTERM
    fprintf(outfile, TGOTO, 0, 0);
#endif

    fputs(CLEAR, outfile);             /* Clear the screen */
    fputs(STANDOUT, outfile);          /* First line inverse video */
    fprintf(outfile, H_STR, "", 0, 0); /* Header line */
    fputs(STANDEND, outfile);          /* End inverse video */
    fputs(LOCK, outfile);              /* lock first line */

/* Go to the end of the screen */
#if TERMCAP
    fprintf(outfile, (char *)tgoto(TGOTO, 0, END));
#endif
#if ANSI
    fprintf(outfile, TGOTO, END + 1, 1);
#endif
#if HPTERM
    fprintf(outfile, TGOTO, END, 0);
#endif

    fflush(outfile);
}

#if TERMCAP
static int mygetstr(char *which, char *where, int need) {
    int retval;
    char *temp, *save, *tgetstr();

    save = where;
    temp = tgetstr(which, &where);
    if (temp == (char *)0) {
        if (need) {
            error(ADLERR_TERMCAP);
            /*NOTREACHED*/
        } else {
            *save  = '\0';
            retval = 0;
        }
    } else {
        *temp  = '\0';
        retval = 1;
    }
    return retval;
}
#endif

void write_head(void) {
    FILE *outfile;

    outfile = stdout;

    if (!header) { return; }

    assertargs("$spec 9", 4); /* ($spec 9 str score moves)	*/

    fputs(NOLOCK, stdout); /* Turn off memory lock		*/

/* Go to the top of the screen */
#if TERMCAP
    fprintf(outfile, (char *)tgoto(TGOTO, 0, 0));
#endif
#if ANSI
    fprintf(outfile, TGOTO, 1, 1);
#endif
#if HPTERM
    fprintf(outfile, TGOTO, 0, 0);
#endif

    fputs(STANDOUT, outfile); /* Inverse video		*/
    fprintf(outfile, H_STR, virtstr(ARG(2)), ARG(3), ARG(4));
    fputs(STANDEND, outfile); /* Normal video			*/
    fputs(LOCK, outfile);     /* Lock the first line		*/

/* Go to the end of the screen */
#if TERMCAP
    fprintf(outfile, (char *)tgoto(TGOTO, 0, END));
#endif
#if ANSI
    fprintf(outfile, TGOTO, END + 1, 1);
#endif
#if HPTERM
    fprintf(outfile, TGOTO, END, 0);
#endif

    fflush(outfile);
}

void head_term(void) {
    fputs(NOLOCK, stdout); /* Turn off memory lock		*/
    fflush(stdout);
}

#endif /* ] !CURSES */

/*** EOF adlscrn.c ***/
