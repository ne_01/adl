#include <stdio.h>

#include "adlrun.h"
#include "adltypes.h"
#include "vstring.h"

int16 foundfile, filenum, linenum;

#define ERRSTR(num, str)                                                       \
    case ADLERR_##num: msg = str; break;
void error(int num) {
#if ERRCHECK
    char *msg;

    switch (num) {
        ERRSTR(DIVZERO, "Divide by zero")
        ERRSTR(NUMARGS, "Too few arguments")
        ERRSTR(BUILTIN, "Illegal builtin routine")
        ERRSTR(SDEM, "Illegal routine for $sdem")
        ERRSTR(SFUS_ROUT, "Illegal routine for $sfus")
        ERRSTR(PROMPT, "Illegal routine for $prompt")
        ERRSTR(ACTOR, "Illegal object for $actor")
        ERRSTR(OVERFLOW, "Stack overflow")
        ERRSTR(UNDERFLOW, "Stack underflow")
        ERRSTR(BADROUT, "Illegal routine call")
        ERRSTR(BADINSTR, "Illegal instruction")
        ERRSTR(BADROUT2, "Illegal routine call")
        ERRSTR(GLOB, "Illegal global for $glob")
        ERRSTR(SETG, "Illegal global for $setg")
        ERRSTR(SETP_OBJ, "Illegal object for $setp")
        ERRSTR(SETP_PROP, "Illegal propnum for $setp")
        ERRSTR(MOVE, "Illegal object for $move")
        ERRSTR(PROP_OBJ, "Illegal object for $prop")
        ERRSTR(PROP_PROP, "Illegal propnum for $prop")
        ERRSTR(VSET_VERB, "Illegal verb for $vset")
        ERRSTR(VSET_PROP, "Illegal propnum for $vset")
        ERRSTR(VPROP_VERB, "Illegal verb for $vprop")
        ERRSTR(VPROP_PROP, "Illegal propnum for $vprop")
        ERRSTR(SPEC, "Illegal parameter for $spec")
        ERRSTR(NAME, "Illegal object for $name")
        ERRSTR(MISS, "Illegal rout for $miss")
        ERRSTR(MEMORY, "Out of memory")
        ERRSTR(EXIT, "Illegal exitcode")
        ERRSTR(TERMCAP, "Insufficient terminal definition")
        ERRSTR(SFUS_ACTOR, "Illegal object for $sfus")
        ERRSTR(SPEC_11, "Illegal actor for $spec 11")
        ERRSTR(TTY, "Unable to open tty")
        ERRSTR(SPEC_12, "Illegal actor for $spec 12")
        ERRSTR(VERB_VEC, "Illegal verb vector size")
        default: msg = "Unknown error number"; break;
    }
    fputs(msg, stderr);
#else
    fprintf(stderr, "Error #%d", num);
#endif
    if (foundfile) {
        fprintf(stderr, ", file \"%s\", line %d", virtstr(filenum), linenum);
    }
    fprintf(stderr, ", ip = %ld\n", (long)ip);
    head_term();
    exit(num);
}

/*** EOF adlerr.c ***/
