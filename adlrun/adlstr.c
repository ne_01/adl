#include <stdio.h>

#include "adldef.h"
#include "adlprog.h"
#include "adlrun.h"
#include "adltypes.h"
#include "builtins.h"
#include "vstring.h"

void eqstring(void) {
    char *s;

    assertargs("$eqst", 2);
    s = malloc(strlen(virtstr(ARG(1))) + 1);
    if (s == (char *)0) { error(ADLERR_MEMORY); }
    strcpy(s, virtstr(ARG(1)));
    if (strcmp(s, virtstr(ARG(2)))) {
        RETVAL = 0;
    } else {
        RETVAL = 1;
    }
    free(s);
}

void substring(void) {
    char *s;
    int t;

    assertargs("$subs", 3);
    t = strlen(virtstr(ARG(1)));
    s = malloc(t + 1);
    if (s == (char *)0) { error(ADLERR_MEMORY); }
    strcpy(s, virtstr(ARG(1)));
    if (ARG(2) >= t) {
        ARG(2) = t;
        ARG(3) = 1;
    } else if (((ARG(2) + ARG(3)) >= t) || (!ARG(3))) {
        ARG(3) = t - ARG(2);
    }
    strncpy(s, &s[ARG(2)], ARG(3));
    s[ARG(3)] = '\0';
    RETVAL    = newtstr(s);
    free(s);
}

void lengstring(void) { RETVAL = strlen(virtstr(ARG(1))); }

void readstring(void) {
    char s[SLEN];

    getstring(s);
    RETVAL = newtstr(s);
}

void catstring(void) {
    char *s;

    assertargs("$cat", 2);
    s = malloc(strlen(virtstr(ARG(1))) + strlen(virtstr(ARG(2))) + 1);
    if (s == (char *)0) { error(ADLERR_MEMORY); }
    strcpy(s, virtstr(ARG(1)));
    strcat(s, virtstr(ARG(2)));
    RETVAL = newtstr(s);
    free(s);
}

void chrstring(void) {
    char s[2];

    assertargs("$chr", 1);
    s[0]   = (char)ARG(1);
    s[1]   = '\0';
    RETVAL = newtstr(s);
}

void ordstring(void) {
    char temp;

    assertargs("$ord", 1);
    temp   = *(virtstr(ARG(1)));
    RETVAL = (int16)temp;
}

int16 strpos(char *s1, char *s2) {
    char *t0, *t1, *t2;

    t0 = s2;
    while (*s2) {
        if (*s1 == *s2) {
            t1 = s1;
            t2 = s2;
            while (*s1 && *s1 == *s2) {
                s1++;
                s2++;
            }
            if (!*s1) /* Found it! */ { return (int16)(s2 - t0); }
            s1 = t1;
            s2 = t2;
        }
        s2++;
    }
    return -1;
}

void posstring(void) {
    char *s;

    assertargs("$pos", 2);
    s = malloc(strlen(virtstr(ARG(1))) + 1);
    if (s == (char *)0) { error(ADLERR_MEMORY); }
    strcpy(s, virtstr(ARG(1)));
    RETVAL = strpos(s, virtstr(ARG(2)));
    free(s);
}

void savestr(void) {
    assertargs("$savestr", 1);
    RETVAL = vs_save(ARG(1));
}

void do_str(void) {
    char s[SLEN];

    assertargs("$str", 1);
    (void)sprintf(s, "%d", ARG(1));
    RETVAL = newtstr(s);
}

void do_num(void) {
    assertargs("$num", 1);
    RETVAL = atoi(virtstr(ARG(1)));
}

void do_name(void) {
    char s[SLEN];
    int16 t;

    assertargs("$name", 1);
#if ERRCHECK
    if ((ARG(1) < 0) || (ARG(1) > NUMOBJ)) { error(ADLERR_NAME); }
#endif
    t = objspace[ARG(1)].adj;
    if (t < 0) {
        strncpy(s, findone(VERB, -t), SLEN);
        strcat(s, " ");
    } else if (t) {
        strncpy(s, findone(ADJEC, t), SLEN);
        strcat(s, " ");
    } else {
        *s = '\0';
    }
    strcat(s, findone(NOUN, objspace[ARG(1)].noun));
    RETVAL = newtstr(s);
}

void do_vname(void) {
    assertargs("$vname", 1);
    RETVAL = newtstr(findone(VERB, ARG(1)));
}

void do_mname(void) {
    assertargs("$mname", 1);
    if (ARG(1) < 0) {
        ARG(1) = -ARG(1);
        do_vname();
    } else if (ARG(1)) {
        RETVAL = newtstr(findone(ADJEC, ARG(1)));
    } else {
        RETVAL = newtstr("");
    }
}

void do_pname(void) {
    assertargs("$pname", 1);
    RETVAL = newtstr(findone(PREP, ARG(1)));
}

void do_define(void) {
    char s1[SLEN], s2[SLEN];

    assertargs("$define", 2);
    strncpy(s1, virtstr(ARG(1)), SLEN);
    strncpy(s2, virtstr(ARG(2)), SLEN);
    define(s1, s2);
}

void do_undef(void) {
    assertargs("$undef", 1);
    undef(virtstr(ARG(1)));
}

/*** EOF adlstr.c ***/
