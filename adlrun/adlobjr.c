#include <stdio.h>

#include "adlprog.h"
#include "adlrun.h"
#include "adltypes.h"
#include "builtins.h"

void setp(void) {
    int16 a, b; /* Arg1, arg2 */

    assertargs("$setp", 3);
    a = ARG(1);
    b = ARG(2);
    if ((a < 0) || (a > NUMOBJ)) { error(ADLERR_SETP_OBJ); }
    if ((b < 1) || (b > _ACT)) { error(ADLERR_SETP_PROP); }
    if ((b >= 1) && (b <= NUMBITS)) {
        if (ARG(3)) {
            objspace[a].bitprops |= bitpat[b - 1];
        } else {
            objspace[a].bitprops &= ibitpat[b - 1];
        }
    } else if ((b > NUMBITS) && (b <= _ACT)) {
        objspace[a].props[b - (NUMBITS + 1)] = ARG(3);
    }
}

void move_obj(void) {
    int16 a, b, t; /* Arg1, arg2, temp */

    assertargs("$move", 2);
    a = ARG(1);
    b = ARG(2);
    if ((a < 0) || (b < 0) || (a > NUMOBJ) || (b > NUMOBJ)) {
        error(ADLERR_MOVE);
    }
    t = objspace[a].loc;
    if (objspace[t].cont != a) {
        t = objspace[t].cont;
        while (objspace[t].link != a) {
            t = objspace[t].link;
        }
        objspace[t].link = objspace[objspace[t].link].link;
    } else {
        objspace[t].cont = objspace[objspace[t].cont].link;
    }
    objspace[a].loc  = b;
    objspace[a].link = 0;
    t                = objspace[b].cont;
    if (t) {
        while (objspace[t].link) {
            t = objspace[t].link;
        }
        objspace[t].link = a;
    } else {
        objspace[b].cont = a;
    }
}

void objprop(int16 n) {
    assertargs("$prop", 1);
    if ((ARG(1) < 0) || (ARG(1) > NUMOBJ)) { error(ADLERR_PROP_OBJ); }
    if ((n >= 1) && (n <= NUMBITS)) {
        if (objspace[ARG(1)].bitprops & bitpat[n - 1]) {
            RETVAL = 1;
        } else {
            RETVAL = 0;
        }
    } else if ((n > NUMBITS) && (n <= _ACT)) {
        RETVAL = objspace[ARG(1)].props[n - (NUMBITS + 1)];
    } else {
        switch (n) {
            case _LOC: RETVAL   = objspace[ARG(1)].loc; break;
            case _CONT: RETVAL  = objspace[ARG(1)].cont; break;
            case _LINK: RETVAL  = objspace[ARG(1)].link; break;
            case _MODIF: RETVAL = objspace[ARG(1)].adj; break;
            default: error(ADLERR_PROP_PROP);
        }
    }
}

void vset(void) {
    assertargs("$vset", 3);
    if ((ARG(1) < 0) || (ARG(1) > NUMVERB)) { error(ADLERR_VSET_VERB); }
    switch (ARG(2)) {
        case _PREACT: verbspace[ARG(1)].preact = ARG(3); break;
        case _ACT: verbspace[ARG(1)].postact   = ARG(3); break;
        default: error(ADLERR_VSET_PROP);
    }
}

void vprop(void) {
    assertargs("$vprop", 2);
    if ((ARG(1) < 0) || (ARG(1) > NUMVERB)) { error(ADLERR_VPROP_VERB); }
    switch (ARG(2)) {
        case _PREACT: RETVAL = verbspace[ARG(1)].preact; break;
        case _ACT: RETVAL    = verbspace[ARG(1)].postact; break;
        default: error(ADLERR_VPROP_PROP);
    }
}

/*** EOF adlobjr.c ***/
