#include <stdio.h>

#include "adlprog.h"
#include "adlrun.h"
#include "adltypes.h"
#include "builtins.h"

void setverb(void) {
    int16 i;

    assertargs("$setv", vecsize);
    for (i = 0; i < vecsize; i++) {
        vecverb[i] = ARG(i + 1);
    }
    RETVAL = 0;
}

void hitverb(void) {
    int16 i;

    assertargs("$hit", vecsize + 1);
    for (i = 0; i < vecsize; i++) {
        if (vecverb[i] == Verb) {
            ARG(2) = ARG(i + 2);
            if (ARG(2)) { move_obj(); }
            RETVAL = 0;
            return;
        }
    }
    RETVAL = 0;
}

void missverb(void) {
    int16 i, oldbp, which;

    assertargs("$miss", vecsize);
    for (i = 0; i < vecsize; i++) {
        if (vecverb[i] == Verb) {
            popip();
            oldbp = pop();
            which = ARG(i + 1);
#if ERRCHECK
            if ((which < 0) || (which > NUMROUT)) { error(ADLERR_MISS); }
#endif
            sp = bp; /* Cut args off stack */
            if (which && routspace[which]) {
                push(1);     /* stackdepth */
                push(oldbp); /* bp */
                puship();    /* ip */
                ip = routspace[which];
            } else {
                push(0);
                bp = oldbp;
            }
            return;
        }
    }
    popip();
    oldbp     = pop();
    sp        = bp + 1;
    stack[bp] = 0;
    bp        = oldbp;
}

/*** EOF adltrans.c ***/
