#include <stdio.h>

#include "adlprog.h"
#include "adlrun.h"
#include "adltypes.h"
#include "builtins.h"

int16 myrand(int16 n) { return 1 + (RAND % n); }

void do_div(void) {
    assertargs("$div", 2);
    if (ARG(2) == 0) { error(ADLERR_DIVZERO); }
    RETVAL = ARG(1) / ARG(2);
}

void do_mod(void) {
    assertargs("$mod", 2);
    if (ARG(2) == 0) { error(ADLERR_DIVZERO); }
    RETVAL = ARG(1) % ARG(2);
}

void do_and(void) {
    int16 i, /* Loop counter */
      tval;  /* Temporary save area */

    tval = -1; /* Bit pattern of all ones */
    for (i = 1; i < RETVAL; i++) {
        tval &= ARG(i);
    }
    RETVAL = tval;
}

void do_or(void) {
    int16 i, /* Loop counter */
      tval;  /* Storage area */

    tval = 0; /* Bit pattern of all zeros */
    for (i = 1; i < RETVAL; i++) {
        tval |= ARG(i);
    }
    RETVAL = tval;
}

/*** EOF adlarith.c ***/
